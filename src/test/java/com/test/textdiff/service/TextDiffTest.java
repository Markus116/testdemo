package com.test.textdiff.service;

import com.test.textdiff.vo.TextDiffVO;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class TextDiffTest {

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private TextDiffService diffService;

    private final String originalText = "Some\n" +
            "Simple\n" +
            "Text\n" +
            "File\n";

    private final String revisedText = "Another\n" +
            "Text\n" +
            "File\n" +
            "With\n" +
            "Additional\n" +
            "Lines\n";

    @Test
    public void diffServiceTest() throws Exception {
        List<TextDiffVO> diffRows = diffService.getDiffRows(originalText, revisedText);
        assertNotNull("Diffs are not null",diffRows);
        assertThat("Diffs are not empty",diffRows.size(), notEmptySize());
        assertThat("Diff size is correct", diffRows.size(), is(7));

        assertThat(diffRows.get(0).getOperation(), is(2));
        assertThat(diffRows.get(1).getOperation(), is(-1));
        assertThat(diffRows.get(2).getOperation(), is(0));
        assertThat(diffRows.get(3).getOperation(), is(-0));
        assertThat(diffRows.get(4).getOperation(), is(1));
        assertThat(diffRows.get(5).getOperation(), is(1));
        assertThat(diffRows.get(6).getOperation(), is(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsDifferentExceptionThanExpected() throws IllegalArgumentException, IOException {
        diffService.getDiffRows(originalText, "");
    }

    private Matcher<Integer> notEmptySize() {
        return is(not(0));
    }
}
