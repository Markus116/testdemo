package com.test.textdiff.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

@Configuration
@PropertySource("classpath:/application.properties")
public class ApplicationConfiguration {
    @Bean
    public ServletContextListener listener() {
        return new ServletContextListener() {
            @Override
            public void contextInitialized(ServletContextEvent sce) {
            }

            @Override
            public void contextDestroyed(ServletContextEvent sce) {}
        };
    }
}
