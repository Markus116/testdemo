package com.test.textdiff.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

class BaseRestController {

    static final String ROOT = "api/";
    final static String RESPONSE_TYPE = MediaType.APPLICATION_JSON_UTF8_VALUE;
    final static String REQUEST_TYPE = MediaType.APPLICATION_JSON_UTF8_VALUE;

    protected final <T> ResponseEntity<T> toSuccessResponse(final T value) {
        return new ResponseEntity<>(value, HttpStatus.OK);
    }

    protected final ResponseEntity emptyOk() {
        return new ResponseEntity(HttpStatus.OK);
    }
}
