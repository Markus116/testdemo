package com.test.textdiff.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class WebAppController {

    @RequestMapping({"/"})
    public String indexRoot(HttpServletRequest request) {
        return "redirect:app/";
    }

    @RequestMapping({"app/", "/app/**"})
    public String index(HttpServletRequest request) {
        return "/index.html";
    }

}
