package com.test.textdiff.controller;

import com.test.textdiff.service.TextDiffService;
import com.test.textdiff.vo.TextDiffVO;
import com.test.textdiff.vo.TextsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = BaseRestController.ROOT, produces = BaseRestController.RESPONSE_TYPE, consumes = BaseRestController.REQUEST_TYPE)
public class TextDiffController extends BaseRestController {

    private static final String DIFF_ROOT = "diff";

    private final TextDiffService service;

    @Autowired
    public TextDiffController(TextDiffService service) {
        this.service = service;
    }

    @CrossOrigin
    @RequestMapping(value = {DIFF_ROOT, DIFF_ROOT + "/"}, method = RequestMethod.POST)
    public ResponseEntity<List<TextDiffVO>> getTextDiff(@RequestBody TextsVO textVO) throws IOException {
        List<TextDiffVO> resultList = service.getDiffRows(textVO.getOriginalText(), textVO.getRevisedText());
        if (textVO.isValid()) {
            return toSuccessResponse(resultList);
        } else {
            throw new IllegalArgumentException("Invalid parameters");
        }
    }
}
