package com.test.textdiff.service;

import com.test.textdiff.vo.Operations;
import com.test.textdiff.vo.TextDiffVO;
import difflib.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TextDiffService {

    private List<Delta<String>> getDeltas(List<String> originalTextLines, List<String> revisedTextLines) throws IOException {
        final Patch<String> patch = DiffUtils.diff(originalTextLines, revisedTextLines);
        return patch.getDeltas();
    }

    private DiffRowGenerator getDiffRowGenerator() {
        return new DiffRowGenerator.Builder().build();
    }

    private TextDiffVO getRowDiff(DiffRow row, int left, int right) {
        if (row.getTag().equals(DiffRow.Tag.INSERT)) {
            return new TextDiffVO(Operations.INSERT.getValue(), row.getNewLine(), left, right, row.getTag().toString());
        } else if (row.getTag().equals(DiffRow.Tag.DELETE)) {
            return new TextDiffVO(Operations.REMOVE.getValue(), row.getOldLine(), left, right, row.getTag().toString());
        } else if (row.getTag().equals(DiffRow.Tag.CHANGE)) {
            final String oldText = row.getOldLine();
            final String newText = row.getNewLine();
            final int operation = getChangeOperation(oldText, newText);
            final String prefix = operation == Operations.CHANGE.getValue() ? "|" : "";
            final String newValue = new StringBuilder().append(oldText).append(prefix).append(newText).toString();
            final DiffRow.Tag changeTag = getChangeTag(oldText, newText);
            return new TextDiffVO(operation, newValue, left, right, changeTag.toString());
        } else {
            throw new IllegalStateException("Unknown pattern tag: " + row.getTag());
        }
    }

    private DiffRow.Tag getChangeTag(String oldText, String newText) {
        if (oldText.isEmpty() || newText.isEmpty()) {
            return oldText.isEmpty() ? DiffRow.Tag.INSERT : DiffRow.Tag.DELETE;
        } else {
            return DiffRow.Tag.CHANGE;
        }
    }

    private int getChangeOperation(String oldText, String newText) {
        if (oldText.isEmpty() || newText.isEmpty()) {
            return oldText.isEmpty() ? Operations.INSERT.getValue() : Operations.REMOVE.getValue();
        } else {
            return Operations.CHANGE.getValue();
        }
    }

    private List<TextDiffVO> transformDiffs(List<TextDiffVO> diffs, List<String> originalTexts) {
        int originalLinesIndex = 0;
        List resultDiffs = new ArrayList<>();
        int diffIndex = 0;
        final int diffSize = diffs.size();
        while (diffIndex < diffSize || originalLinesIndex < originalTexts.size()) {
            TextDiffVO diff = diffIndex < diffSize ? diffs.get(diffIndex) : null;
            if (diff == null || diff.getLeftIndex() > originalLinesIndex) {
                String line = originalTexts.get(originalLinesIndex);
                resultDiffs.add(new TextDiffVO(Operations.EQUAL.getValue(), line, originalLinesIndex, originalLinesIndex, DiffRow.Tag.EQUAL.toString()));
                originalLinesIndex++;
            } else {
                resultDiffs.add(diff);
                if (diff.getOperation() != 1) {
                    originalLinesIndex++;
                }
                diffIndex++;
            }
        }
        return resultDiffs;
    }

    public List<TextDiffVO> getDiffRows(String originalText, String revisedText) throws IOException {
        if (originalText.isEmpty() || revisedText.isEmpty()) {
            throw new IllegalArgumentException("Invalid parameters");
        }

        List<String> originalList = Arrays.asList(originalText.split("\n"));
        List<String> revisedList = Arrays.asList(revisedText.split("\n"));

        final DiffRowGenerator dfg = getDiffRowGenerator();

        final ArrayList textDiffList = new ArrayList<>();

        for (Delta delta : getDeltas(originalList, revisedList)) {
            final List<DiffRow> generateDiffRows = dfg.generateDiffRows(delta.getOriginal().getLines(), delta.getRevised().getLines());

            int leftPos = delta.getOriginal().getPosition();
            int rightPos = delta.getRevised().getPosition();

            for (DiffRow row : generateDiffRows) {
                TextDiffVO textDiff = getRowDiff(row, leftPos, rightPos);
                if (row.getTag().equals(DiffRow.Tag.INSERT) || row.getTag().equals(DiffRow.Tag.CHANGE)) {
                    rightPos++;
                }

                if (row.getTag().equals(DiffRow.Tag.DELETE) || row.getTag().equals(DiffRow.Tag.CHANGE)) {
                    leftPos++;
                }
                textDiffList.add(textDiff);
            }
        }

        return transformDiffs(textDiffList, originalList);
    }
}
