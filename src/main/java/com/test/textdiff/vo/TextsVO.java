package com.test.textdiff.vo;

public class TextsVO {
    private String originalText;
    private String revisedText;

    public TextsVO() {}

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public String getRevisedText() {
        return revisedText;
    }

    public void setRevisedText(String revisedText) {
        this.revisedText = revisedText;
    }

    public boolean isValid() {
        return !this.originalText.isEmpty() && !this.revisedText.isEmpty();
    }
}
