package com.test.textdiff.vo;

public class TextDiffVO {
    private final int operation;
    private final String value;
    private final int leftIndex;
    private final int rightIndex;
    private final String tag;

    public TextDiffVO(int operation, String value, int leftIndex, int rightIndex, String tag) {
        this.operation = operation;
        this.value = value;
        this.leftIndex = leftIndex;
        this.rightIndex = rightIndex;
        this.tag = tag;
    }

    public int getOperation() {
        return operation;
    }

    public String getValue() {
        return value;
    }

    public int getLeftIndex() {
        return leftIndex;
    }

    public int getRightIndex() {
        return rightIndex;
    }

    public String getTag() {
        return tag;
    }
}
