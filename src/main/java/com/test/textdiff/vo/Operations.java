package com.test.textdiff.vo;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Operations {
    REMOVE(-1),
    EQUAL(0),
    INSERT(1),
    CHANGE(2);

    private final int value;

    Operations(int value) {
        this.value = value;
    }

    @JsonValue
    public int getValue() {
        return value;
    }
}
