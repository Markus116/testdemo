import React from "react";
import Table from "react-bootstrap/lib/Table";
import ResultItemRow from "./ResultItemRow";
import ResultItemHeader from "./ResultItemHeader";

import PropTypes from 'prop-types'

const CompareResults = (({results}) => {
    let items = results.map((item, index)=> {
        return (<ResultItemRow key={index}  index={index+1} item={item}></ResultItemRow>);
    });
    return (
        <div>
            <Table>
                <ResultItemHeader></ResultItemHeader>
                <tbody>
                {items}
                </tbody>
            </Table>
        </div>
    );
});

CompareResults.propTypes = {
    results: PropTypes.arrayOf(PropTypes.object),
};


export default CompareResults;