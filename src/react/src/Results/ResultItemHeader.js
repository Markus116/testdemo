import React from 'react';

const ResultItemHeader = (() => {
    return (
        <thead>
            <tr>
                <th>#</th>
                <th>Diff Type</th>
                <th>Text</th>
            </tr>
        </thead>
    );
});

export default ResultItemHeader;