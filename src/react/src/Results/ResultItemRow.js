import React from 'react';
import PropTypes from 'prop-types';

const ResultItemRow = ( ({index, item}) => {
    let operationSign = item.operation === -1 ? " - " : [" ", " + ", " * "][item.operation];
    let cssClass = item.operation === -1 ? "Deleted-row" :
        ["", "Added-row", "Changed-row"][item.operation];
    return (
        <tr className={cssClass}>
            <td>{index}</td>
            <td>{operationSign}</td>
            <td>{item.value}</td>
        </tr>
    );
});

ResultItemRow.propTypes = {
    item: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired
};

export default ResultItemRow;