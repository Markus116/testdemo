import React, { Component } from 'react';
import CompareText from './CompareText';
import './App.css';

class App extends Component {
  render() {
    return (
      <CompareText className="App"></CompareText>
    );
  }
}

export default App;
