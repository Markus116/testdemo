import React from "react";
import Col from "react-bootstrap/lib/Col";
import ControlLabel from "react-bootstrap/lib/ControlLabel";
import FormControl from "react-bootstrap/lib/FormControl";
import FileLoader from "./FileLoader";

import "../App.css";

import PropTypes from 'prop-types';

const TextContainer = ( ({title, text, type, onTextChanged, onFileLoaded, offset}) => {
    return (
        <Col xs={5} md={5} lg={5}
             lgOffset={offset} mdOffset={offset} xsOffset={offset}>
            <ControlLabel>{title}</ControlLabel>
            <FormControl
                className="Compare-text"
                key="oldText"
                componentClass="textarea"
                onChange={onTextChanged}
                placeholder="Place text here"
                value={text}/>
            <FileLoader name={type} onFileLoaded={onFileLoaded}></FileLoader>
        </Col>
    );
});

TextContainer.propTypes = {
    title: PropTypes.string.isRequired,
    text : PropTypes.string,
    type:  PropTypes.string.isRequired,
    onTextChanged: PropTypes.func.isRequired,
    onFileLoaded: PropTypes.func.isRequired,
    offset: PropTypes.number
};

export default TextContainer;
