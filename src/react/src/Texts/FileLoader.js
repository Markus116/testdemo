import React from 'react';

const FileLoader = ( ({onFileLoaded, name}) => {
    const loadFileContent = (event) => {
        let input = event.target;
        var reader = new FileReader();
        reader.onload = function () {
            onFileLoaded( {type:name, text: reader.result} );
        };
        reader.onerror = function(event) {
            console.error("File could not be read! Code " + event.target.error.code);
        };
        reader.readAsText(input.files[0]);
    };

    return (
        <div>
            <input
                type='file' accept='text/plain'
                onChange={loadFileContent}></input>
        </div>
    );
});

export default FileLoader;