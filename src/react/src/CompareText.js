import React, {Component} from "react";
import Button from "react-bootstrap/lib/Button";
import CompareResults from "./Results/CompareResults";
import TextContainer from "./Texts/TextContainer";
import Row from "react-bootstrap/lib/Row";
import Checkbox from "react-bootstrap/lib/Checkbox"
import "./App.css";

import axios from 'axios';

let jsdiff = require('diff');

const API = "http://localhost:8080/api/";

class CompareText extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldText: `Some
Simple
Text
File
`,

            newText: `Another
Text
File
With
Additional
Lines
`,
            compareRes: [],
            serverMode: false
        };

        this.oldTextChanged = this.oldTextChanged.bind(this);
        this.newTextChanged = this.newTextChanged.bind(this);

        this.onFileLoad = this.onFileLoad.bind(this);

        this.compareTexts = this.compareTexts.bind(this);
        this.compareOnTheServer = this.compareOnTheServer.bind(this);
        this.compareOnTheClient = this.compareOnTheClient.bind(this);

        this.onServerModeChanged = this.onServerModeChanged.bind(this);

    }

    compareTexts() {
        if(this.state.serverMode){
            this.compareOnTheServer();
        } else {
            this.compareOnTheClient();
        }
    }

    getDiffOperation(obj) {
        return obj.removed ? -1 : (obj.added ? 1 : 0);
    }

    getDiffItemByInd(obj, index) {
        const values = obj.value.split('\n');
        return index < values.length ? values[index] : null;
    }

    getDiffSubItems(obj) {
        let opValue = this.getDiffOperation(obj);
        let values = obj.value.split('\n');
        values = values.map((item) => {
            return {operation: opValue, value: item};
        });
        values.length = obj.count;
        return values;
    }

    transformItems(diffs) {
        let res = [];
        for (let i = 0; i < diffs.length - 1; i++) {
            let firstDiffArr = this.getDiffSubItems(diffs[i]);
            let secDiffArr = this.getDiffSubItems(diffs[i + 1]);
            if (this.getDiffOperation(diffs[i]) + this.getDiffOperation(diffs[i + 1]) !== 0) {
                res = res.concat(firstDiffArr);
                if (i === diffs.length - 2) {
                    res = res.concat(secDiffArr);
                }
            } else {
                let updatedCOll = this.collectItems(firstDiffArr, secDiffArr);
                res = res.concat(updatedCOll);
                i++;
            }
        }
        return res;
    }

    collectItems(items1, items2) {
        let res = [];
        let ml = Math.max(items1.length, items2.length);
        for (let i = 0; i < ml; i++) {
            let resDiffItem = {};
            if (items1.length > i && items2.length > i) {
                resDiffItem = {operation: 2, value: (items1[i].value + " | " + items2[i].value)};
            } else {
                let item = items1[i] || items2[i];
                resDiffItem = {operation: item.operation, value: item.value};
            }
            res.push(resDiffItem);
        }
        return res;
    }

    oldTextChanged(event) {
        this.setState({oldText: event.target.value});
    }

    newTextChanged(event) {
        this.setState({newText: event.target.value});
    }

    onFileLoad(data) {
        this.setState({[data.type]: data.text});
    }

    onServerModeChanged(){
        const newMode = !this.state.serverMode;
        this.setState({serverMode: newMode});
    }

    compareOnTheClient(){
        let diff = jsdiff.diffTrimmedLines(this.state.oldText, this.state.newText);
        this.setState({compareRes: this.transformItems(diff)});
    }

    compareOnTheServer(){
        axios({
            method: 'post',
            url: API + 'diff/',
            headers: {'accept': 'application/json','content-type': 'application/json'},
            data: {originalText: this.state.oldText, revisedText: this.state.newText}
        }).then((response) => {
            this.setState({compareRes: response.data});
        }).catch((err) => {
            console.log("err", err.message);
        });
    }

    render() {
        return (
            <div className="Compare-container">
                <Row>
                    <TextContainer
                        title={'Old Text'}
                        text={this.state.oldText}
                        type="oldText"
                        onTextChanged={this.oldTextChanged}
                        onFileLoaded={this.onFileLoad}
                        offset={1}>
                    </TextContainer>
                    <TextContainer
                        title={'New Text'}
                        text={this.state.newText}
                        type="newText"
                        onTextChanged={this.newTextChanged}
                        onFileLoaded={this.onFileLoad}
                        offset={0}>
                    </TextContainer>
                </Row>

                <div className="Compare-button">
                    <Checkbox checked={this.state.serverMode}
                              onChange={this.onServerModeChanged}>Server mode</Checkbox>
                    <Button onClick={this.compareTexts}>Compare</Button>
                </div>

                <div className="Compare-results">
                    <CompareResults results={this.state.compareRes}></CompareResults>
                </div>

            </div>
        );
    }
}

export default CompareText;